Typ: Zombie survival

- pohrať sa s "cut scenami" aby bola hra zaujímavejšia
- z 2 dedín spraviť útočište
	- hráč bude môcť založiť klan alebo niečo také
	- vedúci klanu bude môcť "opraviť" mesto a tým ho zmeniť na útočište
	- v útočišti budú obchodníci
	- bude sa dať vylepšovať pomocou predmetov v sklade
	- bude sa v ňom nachádzať spawn point pre členov
	- bude v inom VW - aby mohlo mať viac klanov útočište a aby som nerušil dediny
	- útok na útočište - iny klan - min 1 deň vopred (tj. 24)
	- nejaké automatické obrany typu míny, možno delá - tie by boli komplikované

- inventár
	- zvyšuje sa pomocou batohu
	- možno zvýšiť aj pomocou svalov
	- bude na váhu, pri preťažení bude hráč spomalený, pri max preťažení bude zastavený

- predmety
	- voľne pohodené v budovách, mestách, dedinách
	- autodiely
	- stavebný material
	- zdravotné pomôcky
	- zbrane
	- batohy
	- oblečenie
	- zvýhodňujúce
		- radar
		- mapa

- vozidlá
	- môže byť funkčné alebo poškodené
	- ak je poškodené tak treba nájsť diely na opravu
	- palivo - nech je sranda
	- útočné zbrane ??

- nákaza
	- ak ťa kusne zombie, budeš nakazený
	- ak použiješ okamžite protilátku tak nákaza zmizne
	- ak ju použiješ neskôr, bude nutné protilátku použiť znova, uvidíme kolko krát
	- ak nepoužiješ tak bude uberať HP až do smrti

- smrť
	- predmety sa uložia do "boxu"
	- ktokoľvek ich môže zobrať

- spawn
	- 

- hráči
	- nie sú vidieť na radare
	- možnosť nájsť predmet radar - odhalí hráčov
	- možnosť nájsť predmet mapa - odhalí mapu