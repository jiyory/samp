/*
 *
 * 
 */

/* ABY SA NAHODOU NEVYSKYTOL VIAC KRAT */
#if defined MAIN_ST
#else
#define MAIN_ST
/* OBSAH SCRIPTU */

    /* ========== ACTORS ========== */
    /*
     * Ked sa actor zacne streamovat pre hraca
     */
    public OnActorStreamIn(actorid, forplayerid) {
        return true;
    }

    /*
     * Ked sa actor prestane streamovat pre hraca
     */
    public OnActorStreamOut(actorid, forplayerid) {
        return true;
    }

    /*
     * Ked NPC vidi globalnu spravu (SendClientMessageTpAll)
     */
    /*public OnClientMessage(color, text[]) {
        return true;
    }*/

    /* ========== PLAYERS ========== */
    /*
     * Ked sa hrac zacne streamovat pre hraca
     */
    public OnPlayerStreamIn(playerid, forplayerid) {
        return true;
    }

    /*
     * Ked sa hrac prestane streamovat pre hraca
     */
    public OnPlayerStreamOut(playerid, forplayerid) {
        return true;
    }

    /* ========== VEHICLES ========== */
    /*
     * Ked sa vozidlo zacne streamovat pre hraca
     */
    public OnVehicleStreamIn(vehicleid, forplayerid) {
        return true;
    }

    /*
     * Ked sa vozidlo prestane streamovat pre hraca
     */
    public OnVehicleStreamOut(vehicleid, forplayerid) {
        return true;
    }
/* KONIEC SCRIPTU */
#endif