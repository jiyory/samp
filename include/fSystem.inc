/*
 * Suborovy system ktory si uklada data formou:
 * kluc=hodnota
 * DataOpen("nazov suboru") -> vrati id suboru -> odporucam pouzit pri pripojeni hraca
 * DataClose(id) -> odporucam pouzit pri odpojeni hraca
 * DataCloseAll() -> odporucam pouzit pri ukonceni serveru, alebo este lepsie pri odpojeni posledneho hraca (malo by sa volat automaticky ked sa zatvori posledny subor)
 *
 * DataGet(id, "kluc") - nacita integer
 * DataSet(id, "kluc", data) - ulozi integer
 *
 * DataGetF - float
 * DataSetF
 * DataGetS - string
 * DataSetS
 * 
 *
 */

/* ABY SA NAHODOU NEVYSKYTOL VIAC KRAT */
#if defined FSYSTEM
#else
#define FSYSTEM
/* OBSAH SCRIPTU */

    #include <PawnPlus>

    new Map<_,Map>:_ffiles; // uklada si udaje zo suborov
    new Map:_ffilesNames; // uklada si nazvy suborov -> kvoli zatvoreniu
    new bool:_ffcreated = false; // ci uz je mapa vytvorena
    new _ffid = 0; // takmer unikatne ID

    /* Zinicializuje system - nesmie byt volane vo forku */
    stock fSystemInit() {
        if (!_ffcreated) {
            _ffiles = map_new<_,Map>();
            map_set_ordered<_,Map>(_ffiles, true);
            _ffilesNames = map_new();
            _ffcreated = true;
        }
    }

    /* Vyprazdni system (nezmaze subory) */
    stock fSystemDestroy() {
        if (_ffcreated) {
            DataCloseAll();
            map_delete_deep<_,Map>(_ffiles);
            map_delete_deep(_ffilesNames);
            _ffcreated = false;
        }
    }

    /* Otvori subor, vrati -1 ak sa nepodari */
    stock DataOpen(const fname[]) {
        /* Skontroluje mapu */
        if (!_ffcreated) {
            print("fSystemInit wanst used! Please do it.");
            return -1;
        }
        /* Otvori a spracuje subor */
        new File:f = fopen(fname, io_readwrite);
        if (f) {
            new buff[128];
            new Map:mp = map_new();
            map_set_ordered(mp, true);
            /* Spracuje subor */
            while (fread(f, buff)) {
                strmid(buff, buff, 0, strlen(buff) - 2);
                new pos = strfind(buff, "=", true);
                if (pos != -1) {
                    new key[25];
                    new data[100];
                    strmid(key, buff, 0, pos);
                    strmid(data, buff, pos + 1, strlen(buff));
                    map_str_add_arr(mp, key, data);
                }
            }
            /* Ulozi unikatne ID */
            if (_ffid == -1) {
                ++_ffid;
            }
            map_add<_,Map>(_ffiles, _ffid, mp);
            map_add_arr(_ffilesNames, _ffid, fname, strlen(fname));
            fclose(f);
            ++_ffid;
            return _ffid - 1;
        } else {
            printf("Cannot open: %s", fname);
        }
        return -1;
    }

    /* Ulozi subor ale nezatvori */
    stock Map:DataSave(const id) {
        // save
        new buff[128];
        map_get_arr(_ffilesNames, id, buff);
        new Map:mp = map_get<_,Map>(_ffiles, id);
        new File:f = fopen(buff, io_write);
        if (f) {
            new key[25];
            new data[100];
            for_map(i : mp) {
                iter_get_key_arr(i, key);
                iter_get_arr(i, data);
                format(buff, sizeof(buff), "%s=%s\r\n", key, data);
                fwrite(f,buff);
            }
            fclose(f);
        } else {
            printf("Cannot open: %s", buff);
        }
        return mp;
    }

    /* Zatvori subor */
    stock DataClose(const id) {
        if (map_has_key<_,Map>(_ffiles, id)) {
            new Map:mp = DataSave(id);
            // odstranenie
            map_remove(_ffilesNames, id);
            map_delete(mp);
            map_remove<_,Map>(_ffiles, id);
        }
    }

    /* Zatvori a ulozi vsetky nezatvorene subory */
    stock DataCloseAll() {
        if (_ffcreated) {
            if (map_size<_,Map>(_ffiles) != 0) {
                for_map_of<_,Map>(it : _ffiles)
                {
                    DataClose(iter_get_key<_,Map>(it));
                }
            }
        }
    }

    /* Zisti, ci existuje dany subor */
    stock DataExists(const fname) {
        return fexist(fname);
    }

    /* Zisti, ci existuje dany kluc */
    stock DataKExists(const id, const key[]) {
        return map_has_str_key(map_get<_,Map>(_ffiles, id), key);
    }

    /* Vrati numericku hodnotu */
    stock DataGet(const id, const key[]) {
        if (map_has_key<_,Map>(_ffiles, id)) {
            new buff[100];
            map_str_get_arr(map_get<_,Map>(_ffiles, id), key, buff);
            return strval(buff);
        }
        return -1;
    }

    /* Vrati float hodnotu */
    stock Float:DataGetF(const id, const key[]) {
        if (map_has_key<_,Map>(_ffiles, id)) {
            new buff[100];
            map_str_get_arr(map_get<_,Map>(_ffiles, id), key, buff);
            return floatstr(buff);
        }
        return -1.0;
    }

    /* Vrati text/array */
    stock DataGetS(const id, const key[]) {
        new buff[100];
        if (map_has_key<_,Map>(_ffiles, id)) {
            map_str_get_arr(map_get<_,Map>(_ffiles, id), key, buff);
        }
        return buff;
    }

    /* Ulozi numericku hodnotu */
    stock DataSet(const id, const key[], const data) {
        new buff[100];
        format(buff, sizeof(buff), "%d", data);
        map_str_set_arr(map_get<_,Map>(_ffiles, id), key, buff);
    }

    /* Ulozi float hodnotu */
    stock Float:DataSetF(const id, const key[], const Float:data) {
        new buff[100];
        format(buff, sizeof(buff), "%f", data);
        map_str_set_arr(map_get<_,Map>(_ffiles, id), key, buff);
    }

    /* Ulozi string/array */
    stock DataSetS(const id, const key[], const data[]) {
        new buff[100];
        format(buff, sizeof(buff), "%s", data);
        map_str_set_arr(map_get<_,Map>(_ffiles, id), key, buff);
    }

/* KONIEC SCRIPTU */
#endif