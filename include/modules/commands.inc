/*
 *
 * 
 */

/* ABY SA NAHODOU NEVYSKYTOL VIAC KRAT */
#if defined COMMANDS
#else
#define COMMANDS
/* OBSAH SCRIPTU */

    CMD:pos(const playerid, const params[])
    {
        if(amx_fork()) { // spolu s threaded spusti vlakno - sucastne vykonavanie
            threaded(sync_explicit) {
                new Float:x, Float:y, Float:z;
                GetPlayerPos(playerid, x, y, z);
                SCMLF(playerid, POSITION, x, y, z)
                printf("POS: %.2f, %.2f, %.2f", x, y, z);
            }
        }
        return true;
    }

/* KONIEC SCRIPTU */
#endif