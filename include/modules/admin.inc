/*
 * Admin script
 * 3 urovne
 *      - SUPPORT - bezne prikazy nizkej urovne, mute/warning
 *      - ADMIN - vyssie prikazy urcene k potrestaniu hracov
 *      - MASTER - prikazy na spravu servera
 *
 * Vsetky prikazy robit na ext. vlakne - aby neblokovali server
 * TODO: zablokovat RCON
 */

 /* SUPPORT */
 /*
  * CAR - vytvori vozidlo
  * TPCAR - zobrazi zoznam vozidiel, alebo presunie na auto - ak zada id
  * GET - privola hraca
  * GOTO - presunie k hracovi
  * TP - presunie hraca na poziciu, interie a svet, pripadne na oznacene miesto na mape ak nezada parametre
  * SETWARP - k nemu prikaz WARP pre hracov
  * WEAP - prida zbrane
  */

/* ABY SA NAHODOU NEVYSKYTOL VIAC KRAT */
#if defined ADMIN
#else
#define ADMIN
/* OBSAH SCRIPTU */

    #include "../include/modules/translation.inc"

    #define ADMIN_NONE 0
    #define ADMIN_SUPPORT 1
    #define ADMIN_ADMIN 2
    #define ADMIN_MASTER 3
    #define MAX_ALVL 3

    stock GetPlayerAdminLevel(const playerid) {
        return GetPVarInt(playerid, "AdminLevel");
    }

    stock SetPlayerAdminLevel(const playerid, const level) {
        if (level >= ADMIN_NONE && level <= ADMIN_MASTER) {
            SetPVarInt(playerid, "AdminLevel", level);
        }
        return GetPVarInt(playerid, "AdminLevel");
    }

    stock ALevelName(level) {
        new tmp[15];
        switch (level) {
            case 0: 
                tmp = "Player";
            case 1:
                tmp = "Support";
            case 2:
                tmp = "Admin";
            case 3:
                tmp = "Game Master";
            default:
                tmp = "Unknown";
        }
        return tmp;
    }

    /* ZOZNAM */
    /*
        * - nepovinne
        x - hotovo
    */

    /* NO ADMIN */
    /*
        /admins
        /warp
    */

    /* SUPPORT */
    /*
        /car model *color *color
        /destroy *id - ak -1 tak vsetky
        /eject *id
        /get id
        /goto id
        /gwp id weapon *ammo - da 9999 ammo
        /heal id
        x /kill id
        /mute id time reason
        /repair *id
        /setwarp *vehicles - vytvori warp vozidla ano/nie
        /tpcar *idx - ak nezada idx tak sa zobrazi dialog
        /unmute id
        /warning id reason
    */

    /* ADMIN */
    /*
        /ban id date reason - datum v tvare "01.01.2022"
        /gcash id cash - moze byt aj zaporne
        /kick id reason
        /time time
        /tp *x *y *z **interier ***vw
        /unvip id - zrusi VIP hraca
        /vip id *date - da hracovi VIP do datumu alebo na mesiac (mozno iny cas)
        /weather id
    */

    /* MASTER */
    /*
        x /gmx time reward reason
        x /setlvl id level
    */

    /* ================= IMPLEMENTACIA ===================== */

    /* SUPPORT */
    CMD:anim(const playerid, const params[]) {
        new anim[64], anim2[64];
        if (!sscanf(params, "ss", anim, anim2)) {
            ApplyAnimation(playerid, anim, anim2, 4.1, 1, 1, 1, 1, 1, 1);
        }
        return true;
    }

    CMD:npc(const playerid, const params[]) {
        new Float:x, Float:y, Float:z;
        GetPlayerPos(playerid, x, y ,z);
        new n = AddStaticNPC(10, x + 2, y + 2, z, 0.0);
        StaticNPCMoveTo(n, x + 2, y + 20, z);
        return true;
    }

    CMD:kill(const playerid, const params[]) {
        if (GetPlayerAdminLevel(playerid) < ADMIN_SUPPORT) return SCML(playerid, ACMD_LOW_LEVEL);
        new player;
        if (sscanf(params, "u", player)) return SCML(playerid, ACMD_WRONG_KILL);
        if (player < 0 || !IsPlayerConnected(player)) return SCML(playerid, ACMD_NOT_CONNECTED);

        SetPlayerHealth(player, 0.0);
        SCMLF(playerid, ACMD_SUCC_KILL, Name(player))
        SCMLF(player, ACMD_SUCC_KILL2, Name(playerid))
        return true;
    }

    /* ADMIN */

    /* MASTER */
    forward adminGmxTimer();
    new gmxId = 0;
    new gmxTime;

    CMD:gmx(const playerid, const params[]) {
        if (GetPlayerAdminLevel(playerid) < ADMIN_MASTER) return SCML(playerid, ACMD_LOW_LEVEL);
        if (gmxId > 0) return SCML(playerid, ACMD_WRONG_GMX_ALREADY);
        new time, reward, reason[64];
        if (sscanf(params, "iis", time, reward, reason)) return SCML(playerid, ACMD_WRONG_GMX);
        if (time < 5 || time > 900) return SCML(playerid, ACMD_WRONG_GMX_TIME); 
        if (reward < 0) return SCML(playerid, ACMD_WRONG_GMX_CASH); 
        if (strlen(reason) < 5) return SCML(playerid, ACMD_WRONG_GMX_REASON);

        gmxId = SetTimer("adminGmxTimer", 1000, true);
        gmxTime = time;

        for (new i = GetPlayerPoolSize(); i >= 0; --i) {
            if (IsPlayerConnected(i)) {
                GiveCash(i, reward);
            }
        }

        SCMLFA(ACMD_SUCC_GMX, Name(playerid), time)
        SCMLFA(ACMD_SUCC_GMX2, reward, reason)
        return true;
    }

    public adminGmxTimer() {
        --gmxTime;

        if (gmxTime >= 60) {
            if (gmxTime % 60 == 0) {
                SCMLFA(ACMD_GMX_MIN, gmxTime / 60)
            }
        } else if (gmxTime > 10) {
            if (gmxTime % 15 == 0) {
                SCMLFA(ACMD_GMX_SEC, gmxTime)
            }
        } else if (gmxTime > 0) {
            SCMLFA(ACMD_GMX_SEC, gmxTime)
        } else if (gmxTime == 0) {
            KillTimer(gmxId);
            SendRconCommand("gmx");
        }
        return true;
    }

    CMD:setlvl(const playerid, const params[]) {
        if (GetPlayerAdminLevel(playerid) < ADMIN_MASTER) return SCML(playerid, ACMD_LOW_LEVEL);
        new player, level;
        if (sscanf(params, "ui", player, level)) return SCML(playerid, ACMD_WRONG_SETLVL);
        if (level > MAX_ALVL || level < 0) return SCML(playerid, ACMD_WRONG_SETLVL_LVL);
        if (player < 0 || !IsPlayerConnected(player)) return SCML(playerid, ACMD_NOT_CONNECTED);

        SetPlayerAdminLevel(player, level);
        SCMLFA(ACMD_SUCC_SETLVL, Name(playerid), Name(player), ALevelName(level))
        return true;
    }

/* KONIEC SCRIPTU */
#endif