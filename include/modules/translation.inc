/*
 * System prekladov
 * 
 * SCML - SendClientMessage Language
 * SCMLA - SendClientMessageToAll Language
 * SCMLF - SendClientMessage Formated Language
 * SCMLFA - SendClientMessageToAll Formated Language
 *
 * Spravy je mozne pridat pomocou ENUMu (riadok +- 14) a naslednym pridanim do includov
 * POZOR! Icludy ktore obsahuju text s diakritikou, musia byt ulozene vo formate Windows 1250
 */

/* ABY SA NAHODOU NEVYSKYTOL VIAC KRAT */
#if defined TRANSLATION
#else
#define TRANSLATION
/* OBSAH SCRIPTU */

    /* OBSAHUJE 'NAZVY' SPRAV */
    enum translation {
        CONNECT,
        DISCONNECT_R0,
        DISCONNECT_R1,
        DISCONNECT_R2,
        POSITION,
        WRONG_CMD,
        ACMD_NOT_CONNECTED,
        ACMD_LOW_LEVEL,
        ACMD_WRONG_SETLVL,
        ACMD_WRONG_SETLVL_LVL,
        ACMD_SUCC_SETLVL,
        ACMD_WRONG_GMX,
        ACMD_WRONG_GMX_TIME,
        ACMD_WRONG_GMX_CASH,
        ACMD_WRONG_GMX_REASON,
        ACMD_SUCC_GMX,
        ACMD_SUCC_GMX2,
        ACMD_GMX_MIN,
        ACMD_GMX_SEC,
        ACMD_WRONG_GMX_ALREADY,
        ACMD_WRONG_KILL,
        ACMD_SUCC_KILL,
        ACMD_SUCC_KILL2,
    }

    enum transdialog {
        LANGUAGE,
        REGISTRATION, // nemozno pouzit slovo REGISTER, pretoze definuje include - nas
        REGISTRATION_AGAIN,
        REGISTRATION_SHORT,
        REGISTRATION_DIFFERENT,
        REGISTRATION_LOGIN,
        LOGIN,
        LOGIN_SHORT,
        LOGIN_WRONG
    }

    enum dialogInfo {
        diaHeader[64],
        diaText[1024],  // maximalna dlzka spravy je 3912 nie 4096!
        diaBtn1[32],
        diaBtn2[32]
    }

    /* INCLUDY SPOJENE S PREKLADMI */
    #include "../include/utils.inc"
    #include "../include/modules/translations/translation_sk.inc"

    /* DEFINUJEME SI PORADIE - POZOR NA MAX */
    #define LANG_SK 0
    #define LANG_CZ 1
    #define LANG_EN 2
    #define LANG_MAX 3 // len urcuje koniec

    /*
     * Vrati jazyk hraca ako cislo
     */
    stock GetPlayerLanguage(const playerid) {
        return GetPVarInt(playerid, "lang");
    }

    /*
     * Nastavi jazyk hraca
     */
    stock SetPlayerLanguage(const playerid, const lang) {
        if (lang >= LANG_SK && lang < LANG_MAX) { // Kontrola ci je jazyk podporovany
            SetPVarInt(playerid, "lang", lang); // Nastavu jazyk
        }
        return GetPVarInt(playerid, "lang"); // Zbytocne ale moze sa hodit ako kontrola, ci sa jazyk nastavil - avsak 0 je pre SK
    }

    /*
     * Posle hracovi prelozenu spravu, sprava musi byt vopred definovana v includoch
     */
    stock SCML(const playerid, const translation:msg) {
        switch (GetPlayerLanguage(playerid)) { // switch je lepsi ako IF, pretoze priamo preskoci kus kodu a nekontroluje kazdu zvlast
            case LANG_SK: {
                _SCM(playerid, messageSK[msg]); // nasa funkcia z "utils" - kontroluje velkost spravy
            }
            case LANG_CZ: {
                _SCM(playerid, messageSK[msg]);
            }
            case LANG_EN: {
                _SCM(playerid, messageSK[msg]);
            }
        }
        return true;
    }

    /*
     * Posle vsetkym hracom prelozenu spravu, sprava musi byt vopred definovana v includoch
     */
    stock SCMLA(const translation:msg) {
        for (new playerid = GetPlayerPoolSize(); playerid >= 0; --playerid) { // teoreticky pri pocte hracov 1000 moze sposobit lagy - treba pouvazovat nad vlaknom
            if (IsPlayerConnected(playerid)) {
                SCML(playerid, msg);
            }
        }
        return true;
    }

    /*
     * Naformatuje spravu a posle hracovi
     */
    #define SCMLF(%1,%2,%3) {new _str[144];\
    switch(GetPlayerLanguage(%1)){\
        case LANG_SK:{format(_str,sizeof(_str),messageSK[%2],%3);}\
        case LANG_CZ:{format(_str,sizeof(_str),messageSK[%2],%3);}\
        case LANG_EN:{format(_str,sizeof(_str),messageSK[%2],%3);}\
    }_SCM(%1,_str);}

    /*
     * Naformatuje spravu a posle vsetkym hracom
     * Formatovanie pre kazdeho hraca moze trvat dlho ak bude vela hracov - uvazovat o zmene funkcie alebo spravit vlakno
     */
    #define SCMLFA(%2,%3) for(new _playerid=GetPlayerPoolSize();_playerid>=0;--_playerid){\
            if(IsPlayerConnected(_playerid)){\
                SCMLF(_playerid,%2,%3)\
            }\
        }

    /*
     * Zobrazi dialog hracovi
     */
    stock SPDL(const playerid, const dialogid, const style, const transdialog:msg) {
        switch (GetPlayerLanguage(playerid)) { // switch je lepsi ako IF, pretoze priamo preskoci kus kodu a nekontroluje kazdu zvlast
            case LANG_SK: {
                ShowPlayerDialog(playerid, dialogid, style, dialogSK[msg][diaHeader], dialogSK[msg][diaText], dialogSK[msg][diaBtn1], dialogSK[msg][diaBtn2]);
            }
            case LANG_CZ: {
                ShowPlayerDialog(playerid, dialogid, style, dialogSK[msg][diaHeader], dialogSK[msg][diaText], dialogSK[msg][diaBtn1], dialogSK[msg][diaBtn2]);
            }
            case LANG_EN: {
                ShowPlayerDialog(playerid, dialogid, style, dialogSK[msg][diaHeader], dialogSK[msg][diaText], dialogSK[msg][diaBtn1], dialogSK[msg][diaBtn2]);
            }
        }
    }

    /*
     * Naformatuje dialog a zobrazi hracovi
     */
    #define SPDLF(%1,%2,%3,%4,%5) {new _str[1024];\
    switch(GetPlayerLanguage(%1)){\
        case LANG_SK:{format(_str,sizeof(_str),dialogSK[%4][diaText],%5);}\
        case LANG_CZ:{format(_str,sizeof(_str),dialogSK[%4][diaText],%5);}\
        case LANG_EN:{format(_str,sizeof(_str),dialogSK[%4][diaText],%5);}\
    }ShowPlayerDialog(%1,%2,%3,dialogSK[%4][diaHeader],_str,dialogSK[%4][diaBtn1],dialogSK[%4][diaBtn2]);}

/* KONIEC SCRIPTU */
#endif