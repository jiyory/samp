/*
 * 
 * 
 */

/*
    * STRUKTURA *
    IP
    HESLO
    SALT
    ADMIN
    POSX
    POSY
    POSZ
    ANGLE
    INTERIOR
    WORLD
    WANTED
    MONEY
    WEAPON 0-11 (strieda sa s AMMO)
    AMMO 0-11 (strieda sa s WEAPON)
    WEAPON_SKILL 0-10 - neda sa getnut - musi byt pvar - GetPlayerSkill

*/

/* ABY SA NAHODOU NEVYSKYTOL VIAC KRAT */
#if defined REGISTER
#else
#define REGISTER
/* OBSAH SCRIPTU */

    #include "../include/modules/translation.inc"
    #include "../include/utils.inc"
    #include "../include/modules/admin.inc"
    #include "../include/fSystem.inc"

    /*
     * Funkcia na zistenie, ci je hrac prihlaseny
     */
    stock IsPlayerLogedIn(const playerid) {
        return GetPVarInt(playerid, "loged") == 1; // vrati true ak sa to rovna 1, inak vrati false
    }

    /*
     * Funkcia na zistenie, ci je hrac prihlaseny
     */
    stock GetPFile(const playerid) {
        return GetPVarInt(playerid, "file"); // vrati true ak sa to rovna 1, inak vrati false
    }

    /*
     * Vrati udaje o hracovi
     */
    stock GetPData(const playerid, const key[]) {
        return DataGet(GetPFile(playerid), key);
    }
    stock GetPDataF(const playerid, const key[]) {
        return DataGetF(GetPFile(playerid), key);
    }
    stock GetPDataS(const playerid, const key[]) {
        return DataGetS(GetPFile(playerid), key);
    }
    /*
     * Nastavi udaje o hracovi
     */
    stock SetPData(const playerid, const key[], const data) {
        return DataSet(GetPFile(playerid), key, data);
    }
    stock SetPDataF(const playerid, const key[], const Float:data) {
        return DataSetF(GetPFile(playerid), key, data);
    }
    stock SetPDataS(const playerid, const key[], const data[]) {
        return DataSetS(GetPFile(playerid), key, data);
    }

    /*
     * Funkcia na nastavenie hraca, ci je prihlaseny
     */
    stock SetPlayerLogedIn(const playerid, const bool:val) {
        SetPVarInt(playerid, "loged", val ? 1 : 0); // ak je val true, nastavi sa 1, inak sa nastavi 0
        return val;
    }

    /* Vypise spravu pri pripojeni hraca */
    stock Connect(const playerid) {
        SetPVarInt(playerid, "file", -1);
        SCMLFA(CONNECT, Name(playerid)) // vypise pripojenie hraca
        return true;
    }

    /* Vypise spravu pri odpojeni hraca */
    stock Disconnect(const playerid, const reason) {
        Save(playerid, GetPFile(playerid));
        DataClose(GetPFile(playerid));
        switch (reason) {
            case 0: {
                SCMLFA(DISCONNECT_R0, Name(playerid))
            }
            case 1: {
                SCMLFA(DISCONNECT_R1, Name(playerid))
            }
            case 2: {
                SCMLFA(DISCONNECT_R2, Name(playerid))
            }
        }
        return true;
    }

    /* pokusi sa nacitat hracove udaje */
    stock tryLoad(const playerid) {
        if (IsPlayerLogedIn(playerid)) {
            return true;
        }
        new file = GetPFile(playerid);
        if (file == -1) {
            new fname[25 + 15];
            format(fname, sizeof(fname), "players/%s.dat", Name(playerid));
            file = DataOpen(fname);
        }
        if (file != -1) {
            SetPVarInt(playerid, "file", file);
            if (DataKExists(file, "ip")) {
                if (!strcmp(Ip(playerid), DataGetS(file, "ip"), true)) {
                    Load(playerid, file);
                } else {
                    SPDLF(playerid, LOGIN, DIALOG_STYLE_PASSWORD, LOGIN, Name(playerid))
                }
            } else {
                // jeho ucet asi neexistuje
                SPDL(playerid, LANGUAGE, DIALOG_STYLE_LIST, LANGUAGE);
            }
        }
        SetPVarInt(playerid, "file", file);
        return false;
    }

    /* nacita udaje hraca */
    stock Load(const playerid, const file) {
        SetPlayerLogedIn(playerid, true);
        GiveCash(playerid, DataGet(file, "cash"));
        SetPlayerAdminLevel(playerid, DataGet(file, "admin"));
        SetPlayerWantedLevel(playerid, DataGet(file, "wanted"));
        // Weapons
        new wp[10];
        new am[10];
        for (new i = 0; i < 12; ++i) {
            format(wp, sizeof(wp), "weap%d", i);
            format(am, sizeof(am), "ammo%d", i);
            GivePlayerWeapon(playerid, DataGet(file, wp), DataGet(file, am));
        }
        // Weapon skills
        new skill[10];
        for (new i = 0; i < 11; ++i) {
            format(skill, sizeof(skill), "skill%d", i);
            SetPlayerSkill(playerid, i, DataGet(file, skill));
        }
        SpawnPlayer(playerid);
        return true;
    }

    /* Ulozi udaje o hracovi */
    stock Save(const playerid, const file) {
        if (IsPlayerLogedIn(playerid)) {
            DataSetS(file, "ip", Ip(playerid));
            DataSet(file, "admin", GetPlayerAdminLevel(playerid));
            new Float:x, Float:y, Float:z, Float:a;
            GetPlayerPos(playerid, x, y, z);
            GetPlayerFacingAngle(playerid, a);
            DataSetF(file, "posx", x);
            DataSetF(file, "posy", y);
            DataSetF(file, "posz", z);
            DataSetF(file, "posa", a);
            DataSet(file, "interior", GetPlayerInterior(playerid));
            DataSet(file, "world", GetPlayerVirtualWorld(playerid));
            DataSet(file, "wanted", GetPlayerWantedLevel(playerid));
            DataSet(file, "cash", GetCash(playerid));
            // weapons
            new weapon, ammo, key[25];
            for (new wp = 0; wp < 12; ++wp) {
                GetPlayerWeaponData(playerid, wp, weapon, ammo);
                format(key, sizeof(key), "weap%d", wp);
                DataSet(file, key, weapon);
                format(key, sizeof(key), "ammo%d", wp);
                DataSet(file, key, ammo);
            }
            // weapon skills
            for (new wp = 0; wp < 11; ++wp) {
                format(key, sizeof(key), "skill%d", wp);
                DataSet(file, key, GetPlayerSkill(playerid, wp));
            }
        }
    }

    /* Vygeneruje nahodnych 32 znakov */
    stock radnomString() {
        new _str[33];
        for (new i = 0; i < 32; ++i) {
            _str[i] = 33 + random(80);
        }
        return _str;
    }

    stock Register(const playerid, pwd[]) {
        new file = GetPFile(playerid);
        printf("register %d", file);
        // IP
        DataSetS(file, "ip", Ip(playerid));
        print("register1");
        // heslo
        new salt[33];
        format(salt, sizeof(salt), "%s", radnomString());
        new hash[65];
        new saltHash[65];
        SHA256_PassHash(salt, "shared", saltHash, sizeof(saltHash));
        SHA256_PassHash(pwd, saltHash, hash, sizeof(hash));
        DataSetS(file, "salt", salt);
        DataSetS(file, "pwd", hash);
        // admin
        DataSet(file, "admin", 0);
        // x y z a    // 681.70, -475.73, 16.33 180.00
        DataSetF(file, "posx", 681.70);
        DataSetF(file, "posy", -475.73);
        DataSetF(file, "posz", 16.33);
        DataSetF(file, "posa", 180.00);
        // interior
        DataSet(file, "interior", 0);
        // world
        DataSet(file, "world", 0);
        // wanted
        DataSet(file, "wanted", 0);
        // cash
        DataSet(file, "cash", 500);
        // weapons
        new key[25];
        for (new wp = 0; wp < 12; ++wp) {
            format(key, sizeof(key), "weap%d", wp);
            DataSet(file, key, 0);
            format(key, sizeof(key), "ammo%d", wp);
            DataSet(file, key, 0);
        }
        // weapon skills
        for (new wp = 0; wp < 11; ++wp) {
            format(key, sizeof(key), "skill%d", wp);
            DataSet(file, key, 0);
        }
        print("register end");
    }

    /* Vykonavanie dialogov */
    stock RegisterDialog(const playerid, const dialogid, const response, const listitem, input[]) {
    #pragma unused listitem     // ak sa pouzije tak toto zmazat
        switch (dialogid) {
            case LANGUAGE: {
                if (response) {
                    SetPlayerLanguage(playerid, LANG_SK/*listitem*/); // TODO - ked budu preklady
                    SPDLF(playerid, REGISTRATION, DIALOG_STYLE_PASSWORD, REGISTRATION, Name(playerid))
                }
            }
            case REGISTRATION: {
                if (response) {
                    if (strlen(input) >= 8) {
                        SPDL(playerid, REGISTRATION_AGAIN, DIALOG_STYLE_PASSWORD, REGISTRATION_AGAIN)
                        SetPVarString(playerid, "pwd", input);
                    } else {
                        SPDL(playerid, REGISTRATION, DIALOG_STYLE_PASSWORD, REGISTRATION_SHORT);
                    }
                } else {
                    SPDL(playerid, LANGUAGE, DIALOG_STYLE_LIST, LANGUAGE);
                }
            }
            case REGISTRATION_AGAIN: {
                if (response) {
                    new pwd[128];
                    GetPVarString(playerid, "pwd", pwd, 128);
                    if (strlen(input) >= 8 && !strcmp(input, pwd, false)) {
                        printf("pwd: %s", pwd);
                        Register(playerid, pwd);
                        SPDLF(playerid, LOGIN, DIALOG_STYLE_PASSWORD, REGISTRATION_LOGIN, Name(playerid))
                    } else {
                        SPDL(playerid, REGISTRATION, DIALOG_STYLE_PASSWORD, REGISTRATION_DIFFERENT);
                    }
                } else {
                    SPDLF(playerid, REGISTRATION, DIALOG_STYLE_PASSWORD, REGISTRATION, Name(playerid))
                }
            }
            case LOGIN: {
                if (response) {
                    if (strlen(input) >= 8) {
                        new file = GetPFile(playerid);
                        if (file != -1) { 
                            new hash[65];
                            new saltHash[65];
                            SHA256_PassHash(DataGetS(file, "salt"), "shared", saltHash, sizeof(saltHash));
                            SHA256_PassHash(input, saltHash, hash, sizeof(hash));
                            if (!strcmp(hash, DataGetS(file, "pwd"), false)) {
                                Load(playerid, file);
                            } else {
                                SPDL(playerid, LOGIN, DIALOG_STYLE_PASSWORD, LOGIN_WRONG);
                            }
                        } else {
                            SPDL(playerid, LANGUAGE, DIALOG_STYLE_LIST, LANGUAGE);
                        }
                    } else {
                        SPDL(playerid, LOGIN, DIALOG_STYLE_PASSWORD, LOGIN_SHORT);
                    }
                }
            }
        }
        return true;
    }

/* KONIEC SCRIPTU */
#endif