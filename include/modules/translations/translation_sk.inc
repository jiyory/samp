/*
 * Slovensky preklad
 * 
 */

/* ABY SA NAHODOU NEVYSKYTOL VIAC KRAT */
#if defined TRANSLATION_SK
#else
#define TRANSLATION_SK
/* OBSAH SCRIPTU */

    #include "../include/colors.inc"

    /* Spravy nesmu presiahnut 144 znakov (vratane farieb a formatovania) */
    new const messageSK[translation][] = {
        "[ ! ] {"SYSTEM_INFO"}Hr�� {FFFFFF}%s{"SYSTEM_INFO"} sa pripojil na server.",
        "[ ! ] {"SYSTEM_INFO"}Hr�� {FFFFFF}%s{"SYSTEM_INFO"} opustil server. [ {FFFFFF}strata spojenia{"SYSTEM_INFO"} ]",
        "[ ! ] {"SYSTEM_INFO"}Hr�� {FFFFFF}%s{"SYSTEM_INFO"} opustil server. [ {FFFFFF}odi�iel{"SYSTEM_INFO"} ]",
        "[ ! ] {"SYSTEM_INFO"}Hr�� {FFFFFF}%s{"SYSTEM_INFO"} opustil server. [ {FFFFFF}kick / ban{"SYSTEM_INFO"} ]",
        "[ ! ] {"SYSTEM_INFO"}Tvoja pozicia je %.2f, %.2f, %.2f.",
        "[ ! ] {"SYSTEM_ERROR"}Pr�kaz /%s neexistuje!",
        "[ ! ] {"SYSTEM_ERROR"}Hr�� nie je pripojen�!",
        "[ ! ] {"SYSTEM_ERROR"}Nem� opr�vnenie pou�i� tento pr�kaz!",
        "[ ! ] {"SYSTEM_ERROR"}Nespr�vny form�t! {"SYSTEM_INFO"}/setlvl [id] [level]",
        "[ ! ] {"SYSTEM_ERROR"}Nespr�vny level! {"SYSTEM_INFO"}/setlvl [id] [0-3]",
        "[ A ] {"SYSTEM_INFO"}Hlavn� administr�tor {"SYSTEM_WHITE"}%s{"SYSTEM_INFO"} nastavil hr��ovi {"SYSTEM_WHITE"}%s{"SYSTEM_INFO"} opr�vnenia \"{"SYSTEM_WHITE"}%s{"SYSTEM_INFO"}\".",
        "[ ! ] {"SYSTEM_ERROR"}Nespr�vny form�t! {"SYSTEM_INFO"}/gmx [seconds] [reward] [reason]",
        "[ ! ] {"SYSTEM_ERROR"}Nespr�vny form�t! {"SYSTEM_INFO"}/gmx [5-900] [reward] [reason]",
        "[ ! ] {"SYSTEM_ERROR"}Nespr�vny form�t! {"SYSTEM_INFO"}/gmx [seconds] [reward >= 0] [reason]",
        "[ ! ] {"SYSTEM_ERROR"}Nespr�vny form�t! {"SYSTEM_INFO"}Pre d�vod zadajte aspo� 5 znakov!",
        "[ A ] {"SYSTEM_INFO"}Hlavn� administr�tor {"SYSTEM_WHITE"}%s{"SYSTEM_INFO"} nastavil re�tart serveru za {"SYSTEM_WHITE"}%d{"SYSTEM_INFO"} sek�nd.",
        "[ A ] {"SYSTEM_INFO"}Odmena: {"SYSTEM_WHITE"}%d{"SYSTEM_INFO"}$. D�vod: {"SYSTEM_WHITE"}%s{"SYSTEM_INFO"}.",
        "[ A ] {"SYSTEM_INFO"}Do re�tartu ost�va {"SYSTEM_WHITE"}%d{"SYSTEM_INFO"} min�t!",
        "[ A ] {"SYSTEM_INFO"}Do re�tartu ost�va {"SYSTEM_WHITE"}%d{"SYSTEM_INFO"} sek�nd!",
        "[ ! ] {"SYSTEM_ERROR"}Re�tart serveru u� prebieha!",
        "[ ! ] {"SYSTEM_ERROR"}Nespr�vny form�t! {"SYSTEM_INFO"}/kill [id]",
        "[ A ] {"SYSTEM_INFO"}Zabil si hr��a {"SYSTEM_WHITE"}%s{"SYSTEM_INFO"}!",
        "[ A ] {"SYSTEM_INFO"}Zabil �a pomocn�k {"SYSTEM_WHITE"}%s{"SYSTEM_INFO"}!"
    };

    /*
     *  {
     *      "Nadpis, max 64 znakov",
     *      "Text, max 4096 znakov vratane formatovania",
     *      "Button YES max 32 znakov",
     *      "Button NO max 32 znakov"
     *  }
     */
    new const dialogSK[transdialog][dialogInfo] = {
        {
            "{FFFFFF}Vo�ba jazyka",
            "{009900}Slovensky\n�esky\nAnglicky",
            "{993333}Zvoli�",
            "{999999}Zatvori�"
        },
        {
            "{FFFFFF}Registr�cia",
            "{009900}Vitaj %s.\nTvoja cesta za�ne t�m, �e si vytvor� ��et.\nNajsk�r zadaj svoje nov� heslo.\nPomocou neho sa bude� nesk�r prihlasova� do hry.\n\nMoje nov� heslo je:",
            "{993333}Pokra�ova�",
            "{999999}Sp�"
        },
        {
            "{FFFFFF}Registr�cia",
            "{009900}�spe�ne si zadal svoje heslo.\nAby sme si boli ist�, �e si sa nepom�lil, zadaj svoje heslo znova.\n\nMoje nov� heslo bolo:",
            "{993333}Zaregistrova�",
            "{999999}Sp�"
        },
        {
            "{FFFFFF}Registr�cia",
            "{009900}Tvoje heslo je pr�li� kr�tke. Sk�s pou�i� in�.\n\nMoje nov� heslo je:",
            "{993333}Pokra�ova�",
            "{999999}Sp�"
        },
        {
            "{FFFFFF}Registr�cia",
            "{009900}Tvoje heslo sa nezhoduje s prv�m zadan�m. Sk�s to e�te raz.\nUPOZORNENIE! Teraz zad�va� nov� heslo, neopakuje� p�vodn�!\n\nMoje nov� heslo je:",
            "{993333}Pokra�ova�",
            "{999999}Sp�"
        },
        {
            "{FFFFFF}Prihl�senie",
            "{009900}Registr�cia bola �spe�n� %s.\nTeraz sa m��e� prihl�si�.\n\nZadaj svoje heslo:",
            "{993333}Prihl�si�",
            "{999999}Zatvori�"
        },
        {
            "{FFFFFF}Prihl�senie",
            "{009900}Vitaj %s. Zadaj heslo:",
            "{993333}Prihl�si�",
            "{999999}Zatvori�"
        },
        {
            "{FFFFFF}Prihl�senie",
            "{009900}Zadan� heslo je pr�li� kr�tke!",
            "{993333}Prihl�si�",
            "{999999}Zatvori�"
        },
        {
            "{FFFFFF}Prihl�senie",
            "{009900}Nespr�vne heslo!\n\nSk�s to znova.",
            "{993333}Prihl�si�",
            "{999999}Zatvori�"
        }
    };

/* KONIEC SCRIPTU */
#endif