/*
 *
 * 
 */

/* ABY SA NAHODOU NEVYSKYTOL VIAC KRAT */
#if defined MAIN_OB
#else
#define MAIN_OB
/* OBSAH SCRIPTU */

    /*
     * Ked sa objekt posunie
     */
    public OnObjectMoved(objectid) {
        return true;
    }

    /*
     * Ked hrac edituje pripojeny objekt
     */
    public OnPlayerEditAttachedObject(playerid, response, index, modelid, boneid, Float:fOffsetX, Float:fOffsetY, Float:fOffsetZ, Float:fRotX, Float:fRotY, Float:fRotZ, Float:fScaleX, Float:fScaleY, Float:fScaleZ) {
        return true;
    }

    /*
     * Ked hrac edituje objekt
     */
    public OnPlayerEditObject(playerid, playerobject, objectid, response, Float:fX, Float:fY, Float:fZ, Float:fRotX, Float:fRotY, Float:fRotZ) {
        return true;
    }

    /*
     * Ked sa pohne player objekt
     */
    public OnPlayerObjectMoved(playerid, objectid) {
        return true;
    }

    /*
     * Ked hrac zvoli objekt
     */
    public OnPlayerSelectObject(playerid, type, objectid, modelid, Float:fX, Float:fY, Float:fZ) {
        return true;
    }
/* KONIEC SCRIPTU */
#endif