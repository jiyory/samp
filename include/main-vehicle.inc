/*
 *
 * 
 */

/* ABY SA NAHODOU NEVYSKYTOL VIAC KRAT */
#if defined MAIN_VH
#else
#define MAIN_VH
/* OBSAH SCRIPTU */

    /*
     * Ked sa updatuje trailer - pripoji
     */
    public OnTrailerUpdate(playerid, vehicleid) {
        return true;
    }

    /*
     * Ked sa updatne vozidlo v ktorom nie je sofer (asi ked sa don narazi napr.)
     */
    public OnUnoccupiedVehicleUpdate(vehicleid, playerid, passenger_seat, Float:new_x, Float:new_y, Float:new_z, Float:vel_x, Float:vel_y, Float:vel_z) {
        return true;
    }

    /*
     * Ked sa zmeni stav poskodenia vozidla - dvere, kapota, ...
     */
    public OnVehicleDamageStatusUpdate(vehicleid, playerid) {
        return true;
    }

    /*
     * Ked je vozidlo znicene
     */
    public OnVehicleDeath(vehicleid, killerid) {
        return true;
    }

    /*
     * Ked sa na vozidlo prida modifikacia - tj. komponent
     */
    public OnVehicleMod(playerid,vehicleid,componentid) {
        return true;
    }

    /*
     * Ked sa vozidlu zmeni paintjob
     */
    public OnVehiclePaintjob(playerid, vehicleid, paintjobid) {
        return true;
    }

    /*
     * Ked je vozidlo presprejovane - nevola sa pri vytvoreni
     */
    public OnVehicleRespray(playerid, vehicleid, color1, color2) {
        return true;
    }

    /*
     * Ked sa zapne/vypne sirena
     */
    public OnVehicleSirenStateChange(playerid, vehicleid, newstate) {
        return true;
    }

    /*
     * Ked sa vozidlo spawne
     */
    public OnVehicleSpawn(vehicleid) {
        return true;
    }

    
/* KONIEC SCRIPTU */
#endif